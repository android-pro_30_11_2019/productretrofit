package com.example.productretrofit.api;

import com.example.productretrofit.entityResponse.ProductResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ProductServices {
    @GET("products")
    Call<List<ProductResponse>> GetProducts(@Header("Authorization") String authorization);
    @DELETE("products/{id}")
    Call<ProductResponse> deleteProduct(@Header("Authorization") String authorization, @Path("id") int id);
    @PUT("products/{id}")
    Call<ProductResponse> updateProduct(@Path("id") int id, @Body ProductResponse body);
}
