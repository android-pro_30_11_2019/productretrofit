package com.example.productretrofit.api;

import com.example.productretrofit.entityResponse.UserResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface UserServices {
    @FormUrlEncoded
    @POST("auth/login")
    Call<UserResponse> Login(
            @Field("email") String email,
            @Field("password") String password
    );
    @FormUrlEncoded
    @POST("auth/register")
    Call<UserResponse> Register(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password
    );

}
