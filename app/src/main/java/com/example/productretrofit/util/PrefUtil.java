package com.example.productretrofit.util;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefUtil {
    private static final String token_key = "token";
    private static final String user_status = "status";
    private static final String preference_user = "preference_user";

    //token;
    public static void setToken(String token, Context context){
        if(context != null){
            SharedPreferences preferences = context.getSharedPreferences(preference_user, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(token_key, token);
            editor.apply();
        }
    }
    public static String getToken(Context context){
        if(context != null){
            SharedPreferences preferences = context.getSharedPreferences(preference_user, Context.MODE_PRIVATE);
            return preferences.getString(token_key,null);
        }
        return null;
    }
    //UserStatus;
    public static void setUserStatus(int status, Context context){
        if(context != null){
            SharedPreferences preferences = context.getSharedPreferences(preference_user, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt(user_status, status);
            editor.apply();
        }
    }
    public static int getUserStatus(Context context){
        if(context != null){
            SharedPreferences preferences = context.getSharedPreferences(preference_user, Context.MODE_PRIVATE);
            return preferences.getInt(user_status,1);
        }
        return 1;
    }
}
