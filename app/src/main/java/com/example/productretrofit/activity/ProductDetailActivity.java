package com.example.productretrofit.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.productretrofit.R;
import com.example.productretrofit.api.APIConfig;
import com.example.productretrofit.api.ProductServices;
import com.example.productretrofit.entityResponse.ProductResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProductDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText etProductName,etCost, etProductQTY;
    private TextView tvId;
    private Button btnUpdate;

    Retrofit retrofit;
    ProductServices services;
    ProductResponse productToUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        tvId = findViewById(R.id.tvId);
        etProductName = findViewById(R.id.etProductName);
        etCost = findViewById(R.id.etCost);
        etProductQTY = findViewById(R.id.etProductQTY);
        btnUpdate = findViewById(R.id.btnUpdate);

        btnUpdate.setOnClickListener(this);
        retrofit = new Retrofit.Builder()
                .baseUrl(APIConfig.BaseURl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        services = retrofit.create(ProductServices.class);

        initValue();

    }

    private void initValue() {
        int id = getIntent().getIntExtra("id",0);
        String name = getIntent().getStringExtra("name");
        int cost = getIntent().getIntExtra("cost",0);
        int qty = getIntent().getIntExtra("qty",0);
        productToUpdate = new ProductResponse(cost,qty,name,id);

        //setvalues
        tvId.setText(String.valueOf(id));
        etProductName.setText(name);
        etCost.setText(cost +"");
        etProductQTY.setText(String.valueOf(qty));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnUpdate:
                UpdateProduct();
                break;
        }
    }

    private void UpdateProduct() {
        productToUpdate.setName(etProductName.getText().toString());
        services.updateProduct(productToUpdate.getId(),productToUpdate).enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if(response.isSuccessful()) {
                    Toast.makeText(ProductDetailActivity.this, "product was Successful Update", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                Toast.makeText(ProductDetailActivity.this, "Failed Update", Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }
}
