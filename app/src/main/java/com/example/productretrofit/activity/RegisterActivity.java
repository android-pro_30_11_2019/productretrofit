package com.example.productretrofit.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.productretrofit.api.APIConfig;
import com.example.productretrofit.api.UserServices;
import com.example.productretrofit.databinding.ActivityRegisterBinding;
import com.example.productretrofit.entityResponse.UserResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity {
    private static final String TAG = "RegisterScreen";
    private ActivityRegisterBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.btnLogin.setOnClickListener(v -> {
            String name = binding.etName.getText().toString();
            String email = binding.etEmail.getText().toString();
            String password = binding.etPassword.getText().toString();
            RegisterAccount(name,email,password);
        });
    }

    private void RegisterAccount(String name, String email, String password) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIConfig.BaseURl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        UserServices services = retrofit.create(UserServices.class);
        services.Register(name,email,password).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if(response.isSuccessful()){
                    Log.e(TAG,"Access Token="+response.body().getAccessToken());
                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Log.e(TAG,"Login Error: \nStatus Code:"+response.raw().code()+"\nMessage: "+ response.raw().message());
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Log.e(TAG,call.toString());
                t.printStackTrace();
            }
        });
    }
}
