package com.example.productretrofit.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.productretrofit.MainActivity;
import com.example.productretrofit.R;
import com.example.productretrofit.api.APIConfig;
import com.example.productretrofit.api.UserServices;
import com.example.productretrofit.databinding.ActivityLoginBinding;
import com.example.productretrofit.entityResponse.UserResponse;
import com.example.productretrofit.util.PrefUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "LoginScreen";
    private ActivityLoginBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogin:
                String email = binding.etEmail.getText().toString();
                String password = binding.etPassword.getText().toString();
                CheckLogin(email,password);
                break;
        }
    }

    private void CheckLogin(String email, String password) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIConfig.BaseURl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        UserServices services = retrofit.create(UserServices.class);
        services.Login(email,password).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if(response.isSuccessful()){
                    String token = response.body().getAccessToken();
                    Log.e(TAG,"Access Token="+ token);
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();

                    PrefUtil.setToken(token, LoginActivity.this);
                    PrefUtil.setUserStatus(2,LoginActivity.this);
                }else{
                    Log.e(TAG,"Login Error: \nStatus Code:"+response.raw().code()+"\nMessage: "+ response.raw().message());
                }
            }
            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Log.e(TAG,call.toString());
                t.printStackTrace();
            }
        });
    }
}
