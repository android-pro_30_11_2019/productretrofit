package com.example.productretrofit.recyclerview;

public interface ProductListener {
    void onRecyclerItemClick(int position);
    void onRecyclerItemDeleteClick(int position);
}
