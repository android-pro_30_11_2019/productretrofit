package com.example.productretrofit.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.productretrofit.R;
import com.example.productretrofit.entityResponse.ProductResponse;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductViewHolder> {
    List<ProductResponse> productList;
    private ProductListener recyclerviewlistener;

    public ProductAdapter() {
        this.productList = new ArrayList<>();
    }

    public void setRecyclerviewlistener(ProductListener recyclerviewlistener) {
        this.recyclerviewlistener = recyclerviewlistener;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product,parent,false);
        return new ProductViewHolder(itemView,recyclerviewlistener);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        holder.onbind(productList.get(position));
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public void addMoreItems(List<ProductResponse> productList) {
        this.productList = productList;
        notifyDataSetChanged();
    }

    public void clear() {
        this.productList.clear();
        notifyDataSetChanged();
    }
}
