package com.example.productretrofit.recyclerview;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.productretrofit.R;
import com.example.productretrofit.entityResponse.ProductResponse;

public class ProductViewHolder extends RecyclerView.ViewHolder {
    private TextView tvProductName;
    private ImageView ivdel;
    public ProductViewHolder(@NonNull View itemView,final ProductListener productListener) {
        super(itemView);
        tvProductName = itemView.findViewById(R.id.tvProductName);
        ivdel = itemView.findViewById(R.id.ivdel);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productListener.onRecyclerItemClick(getAdapterPosition());
            }
        });
        ivdel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productListener.onRecyclerItemDeleteClick(getAdapterPosition());
            }
        });
    }

    public void onbind(ProductResponse item) {
        tvProductName.setText(item.getName());
    }
}
