package com.example.productretrofit;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.productretrofit.activity.LoginRegisterActivity;
import com.example.productretrofit.activity.ProductDetailActivity;
import com.example.productretrofit.api.APIConfig;
import com.example.productretrofit.api.ProductServices;
import com.example.productretrofit.entityResponse.ProductResponse;
import com.example.productretrofit.recyclerview.ProductAdapter;
import com.example.productretrofit.recyclerview.ProductListener;
import com.example.productretrofit.util.PrefUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.media.CamcorderProfile.get;

public class MainActivity extends AppCompatActivity implements ProductListener {
    private RecyclerView rvProduct;
    private List<ProductResponse> productList = new ArrayList<>();
    private ProductAdapter adapter = new ProductAdapter();
    private static final String TAG = MainActivity.class.getSimpleName();
    Retrofit retrofit;
    String authorization;
    ProductServices services;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvProduct = findViewById(R.id.rvProduct);

        int status = PrefUtil.getUserStatus(this);
        String token = PrefUtil.getToken(this);
        if(status == 1 && token == null){ //If New User or Logout
            Intent intent = new Intent(this, LoginRegisterActivity.class);
            startActivity(intent);
            finish();
        }
        retrofit = new Retrofit.Builder()
                .baseUrl(APIConfig.BaseURl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        services = retrofit.create(ProductServices.class);
        authorization = "Bearer "+ PrefUtil.getToken(this);
        InitRecyclerViewProduct();
    }

    private void InitRecyclerViewProduct() {
        services.GetProducts(authorization).enqueue(new Callback<List<ProductResponse>>() {
            @Override
            public void onResponse(Call<List<ProductResponse>> call, Response<List<ProductResponse>> response) {
                if(response.isSuccessful()) {
                    //Status: 200
                    productList = response.body();
                    Log.e(TAG, productList.toString());
                    rvProduct.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                    adapter.setRecyclerviewlistener(MainActivity.this);
                    adapter.clear();
                    adapter.addMoreItems(productList);
                    rvProduct.setAdapter(adapter);
                }

                else
                    Log.e(TAG,"Login Error: \nStatus Code:"+response.raw().code()+"\nMessage: "+ response.raw().message());
            }
            @Override
            public void onFailure(Call<List<ProductResponse>> call, Throwable t) {
                Log.e(TAG, "onFailure");
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onRecyclerItemClick(int position) {
        ProductResponse product = productList.get(position);
        Toast.makeText(this, "You click on: "+product.getName(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(MainActivity.this, ProductDetailActivity.class);
        //pass data;
        intent.putExtra("id",product.getId());
        intent.putExtra("name",product.getName());
        intent.putExtra("cost",product.getCost());
        intent.putExtra("qty",product.getQuantity());
        startActivity(intent);
    }

    @Override
    public void onRecyclerItemDeleteClick(int position) {
        Toast.makeText(this, "Delete Click on: "+productList.get(position).getName(), Toast.LENGTH_SHORT).show();
        new AlertDialog.Builder(this)
                .setTitle("Delete Confirm Box")
                .setMessage("Are you sure wanna delete this item?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes,(dialog, which) -> {
                    Toast.makeText(this, "Yes is click", Toast.LENGTH_SHORT).show();
                    services.deleteProduct(authorization,productList.get(position).getId()).enqueue(new Callback<ProductResponse>() {
                        @Override
                        public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                            if(response.isSuccessful()){
                                Toast.makeText(MainActivity.this, "Item deleted", Toast.LENGTH_SHORT).show();
                                InitRecyclerViewProduct();
                            }
                            else
                                Log.e(TAG,"Login Error: \nStatus Code:"+response.raw().code()+"\nMessage: "+ response.raw().message());
                        }

                        @Override
                        public void onFailure(Call<ProductResponse> call, Throwable t) {
                            Log.e(TAG, "onFailure");
                            t.printStackTrace();
                        }
                    });
                })
                .setNegativeButton(android.R.string.no,(dialog, which) -> {
                    Toast.makeText(this, "No is click", Toast.LENGTH_SHORT).show();
                })
                .show();
    }
}
