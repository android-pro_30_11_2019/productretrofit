package com.example.productretrofit.entityResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserResponse{

	@SerializedName("access_token")
	private String accessToken;
	@SerializedName("status")
	private String status;
	@SerializedName("message")
	private String message;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setAccessToken(String accessToken){
		this.accessToken = accessToken;
	}

	public String getAccessToken(){
		return accessToken;
	}

	@Override
	public String toString() {
		return "UserResponse{" +
				"accessToken='" + accessToken + '\'' +
				", status='" + status + '\'' +
				", message='" + message + '\'' +
				'}';
	}
}