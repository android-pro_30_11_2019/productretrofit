package com.example.productretrofit.entityResponse;

import com.google.gson.annotations.SerializedName;

public class ProductResponse{

	@SerializedName("familyId")
	private int familyId;

	@SerializedName("cost")
	private int cost;

	@SerializedName("quantity")
	private int quantity;

	@SerializedName("locationId")
	private int locationId;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	public ProductResponse() {
	}

	public ProductResponse(int cost, int quantity, String name, int id) {
		this.cost = cost;
		this.quantity = quantity;
		this.name = name;
		this.id = id;
		this.familyId = 1;
		this.locationId = 2;
	}

	public void setFamilyId(int familyId){
		this.familyId = familyId;
	}

	public int getFamilyId(){
		return familyId;
	}

	public void setCost(int cost){
		this.cost = cost;
	}

	public int getCost(){
		return cost;
	}

	public void setQuantity(int quantity){
		this.quantity = quantity;
	}

	public int getQuantity(){
		return quantity;
	}

	public void setLocationId(int locationId){
		this.locationId = locationId;
	}

	public int getLocationId(){
		return locationId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"ProductResponse{" + 
			"familyId = '" + familyId + '\'' + 
			",cost = '" + cost + '\'' + 
			",quantity = '" + quantity + '\'' + 
			",locationId = '" + locationId + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}